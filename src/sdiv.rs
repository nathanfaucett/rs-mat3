use number_traits::Num;

use super::zero;

#[inline]
pub fn sdiv<'a, T: Copy + Num>(out: &'a mut [T; 9], a: &[T; 9], s: T) -> &'a mut [T; 9] {
    if s == T::zero() {
        zero(out)
    } else {
        out[0] = a[0] / s;
        out[1] = a[1] / s;
        out[2] = a[2] / s;
        out[3] = a[3] / s;
        out[4] = a[4] / s;
        out[5] = a[5] / s;
        out[6] = a[6] / s;
        out[7] = a[7] / s;
        out[8] = a[8] / s;
        out
    }
}
#[test]
fn test_sdiv() {
    let mut v = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    sdiv(&mut v, &[1, 0, 0, 0, 1, 0, 0, 0, 1], 1);
    assert!(v == [1, 0, 0, 0, 1, 0, 0, 0, 1]);
}
